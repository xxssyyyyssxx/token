package top.jfunc.token;

/**
 * token管理器
 * @author xiongshiyan
 */
public interface TokenManager<M> {
    /**
     * 生成token
     * @see this#getToken(Object)
     * @see this#saveToken(String, Object)
     * @param m 实体
     * @return token值
     */
    default String createToken(M m){
        String token = getToken(m);
        saveToken(token , m);
        return token;
    }

    /**
     * 根据token获取
     * @param token token
     * @return 根据token获取的实体
     */
    M findByToken(String token);
    /**
     * 根据token判断是否存在，本来也可以通过findByToken，但是避免过多的网络传输和序列化
     * @param token token
     * @return 是否存在
     */
    default boolean existByToken(String token){return null != findByToken(token);}

    /**
     * 更新token的过期
     * @param token token
     */
    void updateExpires(String token);

    /**
     * 删除
     * @param token token
     * @return 删除是否成功
     */
    boolean deleteToken(String token);

    /**
     * 产生token
     * @see this#createToken(Object) 
     * @param m 实体
     * @return token
     */
    String getToken(M m);

    /**
     * 保存token和实体的关系
     * @see this#createToken(Object)
     * @param token token
     * @param m 实体
     */
    void saveToken(String token, M m);


    /**
     * 踢人
     * @param m 实体
     * @param newToken 新token
     * @param doMore 还要做的事情
     */
    default void kickingOld(M m, String newToken, Runnable doMore){
        kickingOld(m , newToken);

        if(null != doMore){
            doMore.run();
        }
    }

    /**
     * 踢人
     * @param m 实体
     * @param newToken token
     */
    void kickingOld(M m, String newToken);
}
