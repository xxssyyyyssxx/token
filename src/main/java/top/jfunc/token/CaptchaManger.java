package top.jfunc.token;

import java.io.IOException;

/**
 * 验证码管理器,可由redis或者db实现
 * <M>一般对应登录人实体
 * @author xiongshiyan at 2018/10/8 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public interface CaptchaManger<M> {
    /**
     * 更新验证码
     * @param m 一般对应登录人实体
     * @param captcha 验证码
     */
    void updateCaptcha(M m, String captcha);

    /**
     * 产生一个验证码
     * @param numbers 字符数
     * @return 验证码
     */
    String generateCode(int numbers);

    /**
     * 根据验证码生成图片
     * @param width 宽度
     * @param height 高度
     * @param code 验证码
     * @return 图片字节流
     * @throws IOException IOException
     */
    byte[] generateCodeImage(int width, int height, String code) throws IOException;

    /**
     * 验证验证码是否正确
     * @param m 人
     * @param captcha 验证码
     * @return 是否正确
     */
    boolean verifyCaptcha(M m, String captcha);

    /**
     * 删除验证码
     * @param m 人
     */
    void deleteCaptcha(M m);

    /**
     * 校验及删除验证码
     * @param m 人
     * @param code 验证码
     * @return 校验是否通过
     */
    default boolean verify(M m, String code){
        boolean verifyCaptcha = verifyCaptcha(m, code);
        deleteCaptcha(m);
        return verifyCaptcha;
    }

    /**
     * 产生验证码、保存、生成图片
     * @param member 人
     * @param width 宽
     * @param height 高
     * @param numbers 个数
     * @return 验证码图形
     * @throws IOException IO
     */
    default byte[] generate(M member, int width, int height, int numbers) throws IOException{
        String code = generateCode(numbers);
        updateCaptcha(member , code);
        return generateCodeImage(width , height , code);
    }
}
