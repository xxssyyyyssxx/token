package top.jfunc.token;

/**
 * @author xiongshiyan
 * 密码错误管理器
 * 可以使用Reids实现，可以使用db实现
 */
public interface ErrorCountManager<M> {
    /**
     * 当前此人密码错误次数
     * @param m 人员
     * @return 错误次数
     */
    int currentErrorCount(M m);

    /**
     * 密码错误次数加 1
     * @param m 人员
     * @return 当前的错误次数
     */
    int incrErrorCount(M m);

    /**
     * 是否达到阀值
     * @param currentErrorCount 当前错误次数
     * @return 是否达到错误阀值
     */
    boolean reachThreshold(int currentErrorCount);

    /**
     * 密码正确的时候清除错误次数
     * @param m 人
     */
    void clearErrorCount(M m);
}
