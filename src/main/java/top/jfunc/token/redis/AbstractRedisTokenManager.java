package top.jfunc.token.redis;


import org.springframework.data.redis.core.RedisTemplate;
import top.jfunc.token.TokenManager;

/**
 * 基于redis的token管理器基类
 * @author xiongshiyan at 2018/8/15 , contact me with email yanshixiong@126.com or phone 15208384257
 */
public abstract class AbstractRedisTokenManager<M> implements TokenManager<M> {
    protected RedisTemplate<Object,Object> redisTemplate;

    protected long apiExpires = 24*60*60;

    @SuppressWarnings("unchecked")
    @Override
    public M findByToken(String token) {
        if(null == token){
            return null;
        }
        return (M)redisTemplate.opsForValue().get(token);
    }

    @Override
    public boolean existByToken(String token) {
        return null != token && redisTemplate.hasKey(token);
    }

    @Override
    public boolean deleteToken(String token){
        redisTemplate.delete(token);
        return true;
    }

    public RedisTemplate<Object, Object> getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate<Object, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public long getApiExpires() {
        return apiExpires;
    }

    public void setApiExpires(long apiExpires) {
        this.apiExpires = apiExpires;
    }
}
