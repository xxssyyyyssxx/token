# token
[![](https://jitpack.io/v/com.gitee.xxssyyyyssxx/token.svg)](https://jitpack.io/#com.gitee.xxssyyyyssxx/token)

#### 项目介绍
TokenManager,ErrorCountManager,CaptchaManager

项目用于管理登录使用的TokenManager，错误次数管理器ErrorCountManager，验证码校验管理器


#### 安装教程
maven { url 'https://jitpack.io' }
compile 'com.gitee.xxssyyyyssxx:token:v1.0'